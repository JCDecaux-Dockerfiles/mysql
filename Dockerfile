FROM jcdecaux/rhel:7
MAINTAINER Nicolas Mallet <nicolas.mallet@jcdecaux.com>

COPY mysql57-community-release-el7.rpm /tmp/mysql57-community-release-el7.rpm
RUN rpm -Uvh /tmp/mysql57-community-release-el7.rpm

COPY yum.conf /etc/yum.conf

RUN yum install supervisor mysql-community-server -y \
  && yum clean all

COPY passwd /etc/passwd

RUN runuser -l mysql -c '/usr/sbin/mysqld --initialize-insecure'

COPY supervisord.conf /etc/supervisord.conf

VOLUME ["/var/lib/mysql"]

EXPOSE 3306

CMD ["/usr/bin/supervisord"]
